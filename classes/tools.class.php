<?php

class Tools
{

    private $conn;
    private $nome_tabella = "user";

    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function province()
    {

        $stm = $this->conn->prepare('SELECT * FROM province order by nome_province ASC');
        $stm->execute();
        $array = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            array_push($array, $row);
        }

        return $array;
    }
}
