<?php

class Lavoro
{

    private $conn;


    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function getList($id_cliente, $anno_select, $anno_globale)
    {
        /*  if ($id_cliente == null) {
             $cliente = '';
         } */

       
         if (isset($id_cliente)) {
            $cliente = 'and id_cliente=' . $id_cliente;
        } else {
            $cliente = '';
        }
        // if ($anno_select == null) {
        //     $anno_select = '';
        // }
        if ($anno_select!='undefined') {
            $anno_select = 'and YEAR(scadenza)="' . $anno_select . '"';
        } else {
            $anno_select = '';
        }
        // if ($anno_globale == null) {
        //     $anno_globale = '';
        // }
       
       // echo'SELECT * FROM lavori where YEAR(scadenza)="' . $anno_globale . '"  ' . $cliente . ' ' . $anno_select . '  order by scadenza DESC';
        $stm = $this->conn->prepare('SELECT * FROM lavori where YEAR(scadenza)="' . $anno_globale . '"  ' . $cliente . ' ' . $anno_select . '  order by scadenza DESC');
        $stm->execute();
        $array = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {

            array_push($array, $row);
        }
        return $array;
    }
    public function loadAnagrafica($id, $anno)
    {

        $stm = $this->conn->prepare('SELECT * FROM anagrafica where id=? order by nome ASC');
        $stm->execute([$id]);
        $array = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            array_push($array, $row);
        }
        return $array;
    }

    public function updadteAnagrafica($res)
    {
        //echo "l funzione la chiama";
        try {
            $request = json_decode($res);

            $stm = $this->conn->prepare('UPDATE anagrafica set 
            nome="' . $request->nome . '",
            indirizzo="' . $request->indirizzo . '",
            localita="' . $request->localita . '",
            provincia="' . $request->provincia . '",
            cap="' . $request->cap . '",
            partita_iva="' . $request->partita_iva . '", 
            codice_fiscale="' . $request->codice_fiscale . '", 
            telefono="' . $request->telefono . '", 
            fax="' . $request->fax . '", 
            email="' . $request->email . '", 
            referente="' . $request->referente . '", 
            tel_referente="' . $request->tel_referente . '", 
            email_referente="' . $request->email_referente . '", 
            fornitore="' . $request->fornitore . '", 
            cliente="' . $request->cliente . '", 
            sdi="' . $request->sdi . '", 
            pec="' . $request->pec . '" 
            where id="' . $request->id . '"');
            $stm->execute([]);
            return array('OK');
        } catch (PDOException $e) {
            return array("KO", $e->getMessage());
        }
    }

}
