<?php

class Ruolo
{

    private $conn;
    private $nome_tabella = "user";

    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function getRuoliList()
    {
        $stm = $this->conn->prepare('SELECT * FROM ruolo order by id ASC');
        $stm->execute();
        $array = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            if ($row['assegnabile'] == 1) {
                $row['assegnabile'] = true;
            } else {
                $row['assegnabile'] = false;
            }
            array_push($array, $row);
        }
        return $array;
    }
    public function getRuoliListEdit($id_menu)
    {

        $stm = $this->conn->prepare('SELECT * FROM ruolo order by id ASC');
        $stm->execute();
        $array = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {

            $stm_checked = $this->conn->prepare('SELECT * FROM menu_items_ruoli where id_items=' . $id_menu . ' and id_ruolo=' . $row['id'] . '');
            $stm_checked->execute();
            $count = $stm_checked->rowCount();
            if ($count == 1) {
                ;
                $row['checked'] = true;
            } else {
                $row['checked'] = false;
            }
            array_push($array, $row);
        }
        return $array;
    }
    public function getPagineList()
    {
        $stm = $this->conn->prepare('SELECT * FROM menu_items where genitore=0 order by testo ASC');
        $stm->execute();
        $array = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            array_push($array, $row);
        }
        return $array;
    }
    public function getmenuItemsList($id_ruolo, $posizione)
    {

        if ($posizione != '') {
            $posizione = 'and posizione= "' . $posizione . '"';
        } else {
            $posizione = '';
        }
        $stm = $this->conn->prepare('SELECT *
        FROM menu_items
        INNER JOIN menu_items_ruoli
        ON menu_items.id_menu = menu_items_ruoli.id_items where menu_items.genitore=0 and menu_items_ruoli.id_ruolo="' . $id_ruolo . '" ' . $posizione . ' order by menu_items.id_menu ASC');
        $stm->execute();
        $array = array();
        $ar_ruolo = array();
        // $submenu=array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            // echo $row['ruolo'].'<br>';
            //$ar_ruolo=array($row['ruolo']);
            $ar_ruolo = explode('-', $row['ruolo']);
            if ($row['sottomenu'] == '1') {
                $row['submenu'] = $this->getsubmenuItemsList($row['id_menu'], $id_ruolo);
                $row['items'] = $row['submenu'];
                $row['label'] = $row['testo'];
                $row['routerLink'] = $row['link'];
                $row['icon'] = $row['icona'];
                // print_r($submenu);
                array_push($array, $row);
            } else {
                $row['label'] = $row['testo'];
                $row['routerLink'] = $row['link'];
                $row['icon'] = $row['icona'];
                array_push($array, $row);
            }
            //  print_r($row_ruolo);

        }
        return $array;
    }
    function getsubmenuItemsList($id, $id_ruolo)
    {
        // echo $id;
        // echo 'SELECT * FROM menu_items where genitore='.$id.' order by testo ASC';
        $stm = $this->conn->prepare('SELECT *
        FROM menu_items
        INNER JOIN menu_items_ruoli
        ON menu_items.id_menu = menu_items_ruoli.id_items where menu_items.genitore=' . $id . ' and menu_items_ruoli.id_ruolo="' . $id_ruolo . '" order by menu_items.id_menu ASC');
        $stm->execute();
        $array = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            $row['label'] = $row['testo'];
            $row['items'] = $row['submenu'];
            $row['routerLink'] = $row['link'];
            $row['icon'] = $row['icona'];
            array_push($array, $row);
        }
        return $array;
    }
    function getMenuItemDetail($id_menu)
    {
        // echo $id;
        // echo 'SELECT * FROM menu_items where genitore='.$id.' order by testo ASC';
        $stm = $this->conn->prepare('SELECT * FROM menu_items where id_menu=' . $id_menu . '');
        $stm->execute();
        $array = array();

        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            $ar_ruoli = explode('-', $row['ruolo']);
            /*  $stm_ruolo = $this->conn->prepare('SELECT * FROM menu_items_ruoli where id_items=' . $id_menu . '');
            $stm_ruolo->execute();
            $ar_ruolo = array();
            while ($row_ruolo = $stm_ruolo->fetch(PDO::FETCH_ASSOC)) {
                print_r($row_ruolo);
              
               array_push($ar_ruolo, $row_ruolo);
            }
            print_r($ar_ruolo); */

            array_push($array, $row);
        }

        return $array;
    }
    function getruolimenu($id_menu)
    {
        $stm = $this->conn->prepare('SELECT * FROM menu_items_ruoli where id_items=' . $id_menu . '');
        $stm->execute();
        $array = array();

        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            array_push($array, $row);
        }

        return $array;
    }
    public function addRuolo($res)
    {
        //echo "l funzione la chiama";
        try {
            $request = json_decode($res);
            // print_r($request);
            // echo 'INSERT INTO indirizzi_spedizioni (id_utente,destinatario,riferimento,indirizzo,provincia,cap,localita,telefono,orario_consegna) values('.$request->id_utente.', '.$request->destinatario.', '.$request->riferimento.', '.$request->indirizzo.', '.$request->provinciaAdd.', '.$request->cap.', '.$request->localita.','.$request->telefono.','.$request->orario_consegna.')';
            //  die;
            $stm = $this->conn->prepare('INSERT INTO ruolo (nome) values(?)');
            $stm->execute([$request->nome]);

            return array('OK');
        } catch (PDOException $e) {
            return array("KO", $e->getMessage());
        }
    }
    public function addMenuItem($res)
    {
        //echo "l funzione la chiama";
        try {



            $request = json_decode($res);

                   //print_r($request);


            $ruolo = implode('-', array_values($request->ruoli));

              //print_r($res);
              $sottomenu= $request->sottomenu[0];
              $posizione=$request->posizione->value;
            //  echo $ruolo;
            //  echo $request->icona->properties->name;
             // foreach ($request->genitore as $row) {


              //}



            //echo 'INSERT INTO menu_items (testo,link,genitore,ruolo,sottomenu,posizione,icona) values("'.$request->testo.'","'.$request->link.'",'.$request->genitore->id_menu.',"'.$ruolo.'","'.$sottomenu.'","'.$posizione.'","pi pi-'.$request->icona->properties->name.'")';
            $stm = $this->conn->prepare('INSERT INTO menu_items (testo,link,genitore,ruolo,sottomenu,posizione,icona) values("'.$request->testo.'","'.$request->link.'",'.$request->genitore->id_menu.',"'.$ruolo.'","'.$sottomenu.'","'.$posizione.'","pi pi-'.$request->icona->properties->name.'")');
            $stm->execute([]);

            $id_inserito = $this->conn->lastInsertId();

            foreach ($request->ruoli as $row) {
                $stm = $this->conn->prepare('INSERT INTO menu_items_ruoli (id_items,id_ruolo) values("' . $id_inserito . '","' . $row . '")');
                $stm->execute([]);
            }

            return array('OK');
        } catch (PDOException $e) {
            return array("KO", $e->getMessage());
        }
    }
    public function editMenuItem($res)
    {
        //echo "l funzione la chiama";
        try {
            $request = json_decode($res);

            //$ruolo = implode('-', array_values($request->ruoli_edit['id']));
//
            //  print_r($res);
            // echo 'INSERT INTO indirizzi_spedizioni (id_utente,destinatario,riferimento,indirizzo,provincia,cap,localita,telefono,orario_consegna) values('.$request->id_utente.', '.$request->destinatario.', '.$request->riferimento.', '.$request->indirizzo.', '.$request->provinciaAdd.', '.$request->cap.', '.$request->localita.','.$request->telefono.','.$request->orario_consegna.')';
            //  die;
            // echo 'UPDATE menu_items set testo="' . $request->testo . '",link="' . $request->link . '",link_dev="' . $request->link_dev . '",genitore="' . $request->genitore . '",ruolo="' . $ruolo . '",sottomenu0"' . $request->sottomenu . '" where id_menu="'.$request->id_menu.'"';
            $stm = $this->conn->prepare('UPDATE menu_items set testo="' . $request->testo . '",link="' . $request->link . '",genitore="' . $request->genitore . '",sottomenu="' . $request->sottomenu . '",posizione="' . $request->posizione . '",icona="' . $request->iconaCtrl . '" where id_menu="' . $request->id_menu . '"');
            $stm->execute([]);
            $stm_delete = $this->conn->prepare('DELETE FROM menu_items_ruoli WHERE id_items="' . $request->id_menu . '"');
            $stm_delete->execute([]);

            foreach ($request->ruoli_edit as $row) {
                if ($row->status == 'true') {
                    $stm = $this->conn->prepare('INSERT INTO menu_items_ruoli (id_items,id_ruolo) values("' . $request->id_menu . '","' . $row->id . '")');
                    $stm->execute([]);
                }
            }

            return array('OK');
        } catch (PDOException $e) {
            return array("KO", $e->getMessage());
        }
    }
    public function updateRuolo($id, $value)
    {

        if ($value) {
            $value = 1;
        } else if (!$value) {
            $value = 0;
        }
        try {
            $stm = $this->conn->prepare('UPDATE ruolo set assegnabile="' . $value . '" where id=?');
            $stm->execute([$id]);

            return array('OK');
        } catch (PDOException $e) {
            return array("KO", $e->getMessage());
        }
    }
    public function delItemsMenu($id)
    {
        try {
            $stm = $this->conn->prepare('DELETE FROM menu_items WHERE id_menu = ? ');
            $stm->execute([$id]);
            return array('OK');
        } catch (PDOException $e) {
            return array("KO", $e->getMessage());
        }
    }
}
