<?php

class Anagrafica
{

    private $conn;


    public function __construct($db)
    {
        $this->conn = $db;
    }
    public function getList($tipo, $anno)
    {
        if ($tipo == 'clienti') {
            $tipo = 'cliente="on"';
        }
        if ($tipo == 'fornitori') {
            $tipo = 'fornitore="on"';
        }
       
        $stm = $this->conn->prepare('SELECT * FROM anagrafica where ' . $tipo . ' order by nome ASC');
        $stm->execute();
        $array = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {

            array_push($array, $row);
        }
        return $array;
    }
    public function loadAnagrafica($id,$anno)
    {

        $stm = $this->conn->prepare('SELECT * FROM anagrafica where id=? order by nome ASC');
        $stm->execute([$id]);
        $array = array();
        while ($row = $stm->fetch(PDO::FETCH_ASSOC)) {
            array_push($array, $row);
        }
        return $array;
    }
    
    public function updadteAnagrafica($res)
    {
        //echo "l funzione la chiama";
        try {
            $request = json_decode($res);
            
            $stm = $this->conn->prepare('UPDATE anagrafica set 
            nome="' . $request->nome . '",
            indirizzo="' . $request->indirizzo . '",
            localita="' . $request->localita . '",
            provincia="' . $request->provincia . '",
            cap="' . $request->cap . '",
            partita_iva="' . $request->partita_iva . '", 
            codice_fiscale="' . $request->codice_fiscale . '", 
            telefono="' . $request->telefono . '", 
            fax="' . $request->fax . '", 
            email="' . $request->email . '", 
            referente="' . $request->referente . '", 
            tel_referente="' . $request->tel_referente . '", 
            email_referente="' . $request->email_referente . '", 
            fornitore="' . $request->fornitore . '", 
            cliente="' . $request->cliente . '", 
            sdi="' . $request->sdi . '", 
            pec="' . $request->pec . '" 
            where id="' . $request->id . '"');
            $stm->execute([]);
            return array('OK');
        } catch (PDOException $e) {
            return array("KO", $e->getMessage());
        }
    }
  
}
