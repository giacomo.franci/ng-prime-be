<?php

session_start();
//print_r($_SESSION);

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: application/json');

require_once '../conn.php';

function getQuery()
{
	$postdata = file_get_contents("php://input");
	return json_decode($postdata);
}


if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$req = getQuery();

	switch ($req->request) {
		case "login":

			//print_r($req);

			if ($req->username != "" || $req->password != "") {
				$username = $req->username;
				// md5 encrypted
				$password = hash('sha512', $req->password);
				//$password = $_POST['password'];
				//echo 'SELECT * FROM amministratori WHERE username="'.$username.'" AND password="'.$password.'" ';
				$sql = "SELECT * FROM utenti WHERE username=? AND password=? ";
				$query = $conn->prepare($sql);
				$query->execute(array($username, $password));
				$row = $query->rowCount();
				$fetch = $query->fetch();

				if ($row > 0) {

					$sql_ruolo = "SELECT * FROM ruolo WHERE id=? ";
					$query_ruolo = $conn->prepare($sql_ruolo);
					$query_ruolo->execute(array($fetch['id_ruolo']));
					$fetch_ruolo = $query_ruolo->fetch();


					$session_id = $fetch['id'];
					$session_nome = $fetch['nome'];
					$session_cognome = $fetch['cognome'];
					$session_password = $fetch['password'];
					//$session_salt = $fetch['salt'];
					$session_ruolo = $fetch['ruolo'];
					$session_id_ruolo = $fetch['id_ruolo'];
					$session_assegnabile = $fetch_ruolo['assegnabile'];
					session_regenerate_id();
					$_SESSION['user'] = $fetch['id'];
					$_SESSION['user_id'] = $fetch['id'];
					$_SESSION['name'] = $req->username;
					$_SESSION['nome'] = $fetch['nome'];
					$_SESSION['username'] = $req->username;
					$_SESSION['password'] = $password;
					$_SESSION['id_ruolo'] = $fetch['id_ruolo'];
					$_SESSION['assegnabile'] = $fetch_ruolo['assegnabile'];


					echo json_encode(array("OK", $req->username, $session_id, $session_nome, $session_cognome, $session_ruolo, $session_id_ruolo, $session_assegnabile));
					//print_r($_SESSION);
					//header("location: home.php");
					return;
				} else {
					echo json_encode(array("KO", "password errata!"));
				}
			}
			break;
		case "check":
			//print_r($_SESSION);
			if (isset($_SESSION['name'])) {
				//echo $_SESSION['name'];
				echo json_encode("OK");
			} else {
				echo json_encode(array("KO", "Sessione scaduta ..."));
			}

			break;
		case "logout";
			$_SESSION = array();
			session_destroy();
			session_unset();
			break;
		default:
			break;
	}
}
