<?php
//session_start();

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: application/json');



require 'include/config.php';

$conn = new Database();
$db = $conn->getConnection();

$ruolo = new Ruolo($db);

//print_r($_GET);
if (isset($_GET['request'])) {

    switch ($_GET['request']) {
        case "ruoli":
            $ruoli = $ruolo->getRuoliList();
            echo json_encode($ruoli);
            break;
        case "ruoliedit":
            $ruoliedit = $ruolo->getRuoliListEdit($_GET['id_menu']);
            echo json_encode($ruoliedit);
            break;
        case "pagine":
            $pagine = $ruolo->getPagineList();
            echo json_encode($pagine);
            break;
        case "menuItems":
            $menuItems = $ruolo->getMenuItemsList($_GET['id_ruolo'], '');
            echo json_encode($menuItems);
            break;
        case "menuLeftItems":
            $menuItems = $ruolo->getMenuItemsList($_GET['id_ruolo'], 'top-left');
            echo json_encode($menuItems);
            break;
        case "menuRightItems":
            $menuItems = $ruolo->getMenuItemsList($_GET['id_ruolo'], 'top-right');
            echo json_encode($menuItems);
            break;
        case "menuSideLeftItems":
            $menuItems = $ruolo->getMenuItemsList($_GET['id_ruolo'], 'left');
            echo json_encode($menuItems);
            break;
        case "menuBottomItems":
            $menuItems = $ruolo->getMenuItemsList($_GET['id_ruolo'], 'bottom');
            echo json_encode($menuItems);
            break;
        case "menuItem":
            $menuItem = $ruolo->getMenuItemDetail($_GET['id_menu']);
            echo json_encode($menuItem);
            break;
        case "updateRuolo":
            $ruolo = $ruolo->updateRuolo($_GET['id_ruolo'], $_GET['value']);
            echo json_encode($ruolo);
            break;
        case "ruolimenu":
            $ruolimenu = $ruolo->getruolimenu($_GET['id_menu']);
            echo json_encode($ruolimenu);
            break;
        case "delItemsMenu":
            $menuItems = $ruolo->delItemsMenu($_GET['id_menu']);
            echo json_encode($menuItems);
            break;
    }
}

/* if (isset($_GET['request']) && $_GET['request'] == "user") {

    $detail = $admin->getUsersDetails($_GET['id']);

    //print_r($detail);
    echo json_encode($detail);
} else if (isset($_GET['request']) && $_GET['request'] == "users") {

    //$users = CantinetteClass::getUsers();
    //print_r($users);
    $users = $admin->getUsersList();

    echo json_encode($users);
}
 */