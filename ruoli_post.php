<?php
//session_start();

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: *');
header('Content-Type: application/json');



require 'include/config.php';


$conn = new Database();
$db = $conn->getConnection();

$ruolo = new Ruolo($db);


$postdata = file_get_contents("php://input");

$request = json_decode($postdata);
//print_r($request);

if (isset($request->request)) {
    switch ($request->request) {
        case "addRuolo":
            $res = $ruolo->addRuolo($postdata);
            echo json_encode($res);
            break;
        case "addMenuItems":
            $res = $ruolo->addMenuItem($postdata);
            echo json_encode($res);
            break;
        case "editMenuItems":
            $res = $ruolo->editMenuItem($postdata);
            echo json_encode($res);
            break;
    }
}
